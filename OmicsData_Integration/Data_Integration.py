import pandas as pd
import pyreadr
import numpy as np
from scipy import stats

hmdbr_file = 'hmdbr_lipidmaps.rda'
metabolomics = 'metabolomics.csv'
proteomics = 'proteomics.csv'
wikipathways = 'wikipathways.xlsx'
input_file = 'metabolomics.csv'
filter = 'Bos taurus'
estats_file = 'estats.xlsx'
strain = 'KO'
control = 'WT'
alpha = 0.05


def data_integration(input_file, hmdbr_file, wikipathways_file, filter):

    pd.set_option('display.max_columns', None)
    pd.set_option('display.max_rows', None)

    node_list = []
    type_list = []
    organism_list = []
    int_list = []
    n1_list = []

    file_nodes = open('int_nodes.json', 'a')
    file_links = open('links_file.json', 'a')

    # Lectura de archivos
    try:
        hmdbr_lipidsmap = pyreadr.read_r('Omics_data/{}'.format(hmdbr_file))
        hmdbr = hmdbr_lipidsmap['hmdbr']
        wikipathways_xlsx = pd.read_excel('Omics_data/{}'.format(wikipathways_file))
        wiki_df2 = pd.read_excel('Omics_data/{}'.format(wikipathways_file), 'Interactions')

    except Exception as e:

        print(e)

    # Lectura de input
    if input_file == 'metabolomics.csv':

        metabolomics_csv = pd.read_csv('Omics_data/{}'.format(metabolomics), sep='\t')

    elif input_file == 'proteomics.csv':

        proteomics_csv = pd.read_csv('Omics_data/{}'.format(proteomics), sep=',')


# Condicional para tratar input_file como proteomic o metabolomic data

    if input_file == 'metabolomics.csv':

        for i in metabolomics_csv['Identifier']:

            for j in range(len(hmdbr['keggid'])):

                if i == hmdbr._get_value(j, 'keggid') or i == hmdbr._get_value(j, 'accession'):  # or i == hmdbr._get_value(j, 'chemid') or i == hmdbr._get_value(j, 'chebid') or i == hmdbr._get_value(j, 'biocydid'):

                    accesion_number = hmdbr._get_value(j, 'accession')
                    ChebID = hmdbr._get_value(j, 'chebid')
                    file_links.write('\n ||| HMDB_lipidsmap info ||| \n')
                    file_links.write(str(hmdbr.iloc[j]))

                    for z in range(len(wikipathways_xlsx['HMDB'])):

                        if (accesion_number == wikipathways_xlsx._get_value(z, 'HMDB') or ChebID == wikipathways_xlsx._get_value(z, 'ChEBI')) and filter == wikipathways_xlsx._get_value(z, 'Organism'):

                            node_ID = wikipathways_xlsx._get_value(z, 'GraphId')
                            name = wikipathways_xlsx._get_value(z, 'Name')
                            organism = wikipathways_xlsx._get_value(z, 'Organism')
                            node_list.append(node_ID)
                            type_list.append(name)
                            organism_list.append(organism)
                            file_links.write('\n ||| Wikipathways Info ||| \n')
                            file_links.write(str(wikipathways_xlsx.iloc[z]))

        file_links.close()

        for q in range(len(node_list)):

            for r in range(len(wiki_df2['node1'])):

                if node_list[q] == wiki_df2._get_value(r, 'node1'):
                    node_int = wiki_df2._get_value(r, 'node2')
                    int_list.append(node_int)
                    n1_list.append(node_list[q])

        int_dict = {'Input Node': n1_list, 'Interaction Node': int_list}
        int_df = pd.DataFrame(int_dict)
        file_nodes.write(str(int_df))
        file_nodes.close()

    elif input_file == 'proteomics.csv':

        for w in proteomics_csv['Identifier']:

            for s in range(len(wikipathways_xlsx['Uniprot-TrEMBL'])):

                if w == wikipathways_xlsx._get_value(s, 'Uniprot-TrEMBL'):
                    node_ID = wikipathways_xlsx._get_value(s, 'GraphId')
                    name = wikipathways_xlsx._get_value(s, 'Name')
                    organism = wikipathways_xlsx._get_value(s, 'Organism')
                    node_list.append(node_ID)
                    type_list.append(name)
                    organism_list.append(organism)
                    file_links.write('\n ||| Wikipathways Info ||| \n')
                    file_links.write(str(wikipathways_xlsx.iloc[s]))

        file_links.close()

        for q in range(len(node_list)):

            for r in range(len(wiki_df2['node1'])):

                if node_list[q] == wiki_df2._get_value(r, 'node1'):
                    node_int = wiki_df2._get_value(r, 'node2')
                    int_list.append(node_int)
                    n1_list.append(node_list[q])

        int_dict = {'Input Node': n1_list, 'Interaction Node': int_list}
        int_df = pd.DataFrame(int_dict)
        file_nodes.write(str(int_df))
        file_nodes.close()

    return()


def control_all(estats_file, control, alpha):



    estats_df1 = pd.read_excel('Omics_data/{}'.format(estats_file), sheet_name='Quantificaciones', converters={'Name': str})
    estats_df2 = pd.read_excel('Omics_data/{}'.format(estats_file), sheet_name='Fenotipos', converters={'Sample Name': str, 'Fenotipe': str})

    main_dict = {'Metabolite': [], "Fenotipe": [], "Ctr Fenotipe": [], "p-value": [], "Fold Change": []}

    sample = estats_df1.iloc[0]
    len_s = len(sample) - 1
    strains_list = []
    list_value_CTR = []
    list_value_KO = []
    meta_list = []
    j = 0
    y = 0

    for x in estats_df2['Fenotipe']:
        if x != control:
            strains_list.append(x)
        new_slist = list(dict.fromkeys(strains_list))
        contador = int(len(new_slist))

    while y < (contador):

        for z in range(len(estats_df1['Name'])):
            sample = estats_df1.iloc[z]
            metabolite = estats_df1._get_value(z, 'Name')

            for i in range(0, len_s):
                fenotipe = estats_df2._get_value(i, 'Fenotipe')
                if fenotipe == control:
                    list_value_CTR.append(int(sample[i + 1]))
                    meta_list.append(metabolite)
                elif fenotipe == new_slist[y]:
                    list_value_KO.append(int(sample[i + 1]))

            mean_KO = np.mean(list_value_KO)
            mean_CTR = np.mean(list_value_CTR)
            fold_change = mean_CTR / mean_KO
            n1, n2 = len(list_value_CTR), len(list_value_KO)
            var_sample1, var_sample2 = np.var(list_value_CTR, ddof=1), np.var(list_value_KO, ddof=1)
            var = (((n1 - 1) * var_sample1) + ((n2 - 1) * var_sample2)) / (n1 + n2 - 2)
            std_error = np.sqrt(var * (1.0 / n1 + 1.0 / n2))

            if fold_change < 1:
                fold_change = -1 / fold_change

            # calcula t statistics
            t = abs(mean_KO - mean_CTR) / std_error

            # p value para dos colas. Usaremos este dado que es el que se ajusta mas al problema
            p_two = 2 * (1 - stats.t.cdf(x=t, df=len(list_value_KO)))
            # p value para una cola
            # p_one = 1 - stats.t.cdf(x=t, df=12)

            fenotipe_ctr = new_slist[y]
            fenotipe_smp = control
            main_dict['Metabolite'].append(metabolite)
            main_dict['p-value'].append(p_two)
            main_dict['Fold Change'].append(fold_change)
            main_dict['Fenotipe'].append(fenotipe_ctr)
            main_dict['Ctr Fenotipe'].append(fenotipe_smp)
            list_value_CTR.clear()
            list_value_KO.clear()

        y = y + 1

    main_df1 = pd.DataFrame(main_dict)
    p_value_df = main_df1.loc[main_df1['p-value'] < alpha]
    print(main_df1)
    print(p_value_df)

    return()

# Esta funcion compara todos los grupos entre ellos


def all_all(estats_file, alpha):

    try:
        estats_df1 = pd.read_excel('Omics_data/{}'.format(estats_file), sheet_name='Quantificaciones', converters={'Name': str})
        estats_df2 = pd.read_excel('Omics_data/{}'.format(estats_file), sheet_name='Fenotipos', converters={'Sample Name': str, 'Fenotipe': str})

        proceso = True
        main_dict = {'Metabolite': [], "Fenotipe 1": [], "Fenotipe 2": [], "p-value": [], "Fold Change": []}

        sample = estats_df1.iloc[0]
        len_s = len(sample) - 1
        strains_list = []
        list_value_CTR = []
        list_value_KO = []
        meta_list = []
        main_df1 = []
        y = 0
        j = 1

        # Revisar codigo introducción de datos, el appen de meta_list
        for x in estats_df2['Fenotipe']:

            strains_list.append(x)
            new_slist = list(dict.fromkeys(strains_list))
            contador = int(len(new_slist))

        while (proceso):

            for z in range(len(estats_df1['Name'])):

                sample = estats_df1.iloc[z]
                metabolite = estats_df1._get_value(z, 'Name')

                for i in range(0, len_s):

                    fenotipe = estats_df2._get_value(i, 'Fenotipe')

                    if fenotipe == new_slist[y]:

                        list_value_CTR.append(int(sample[i + 1]))
                        meta_list.append(metabolite)

                    elif fenotipe == new_slist[j]:

                        list_value_KO.append(int(sample[i + 1]))

                fenotipe_ctr = new_slist[y]
                fenotipe_smp = new_slist[j]
                mean_KO = np.mean(list_value_KO)
                mean_CTR = np.mean(list_value_CTR)
                fold_change = mean_CTR / mean_KO
                n1, n2 = len(list_value_CTR), len(list_value_KO)
                var_sample1, var_sample2 = np.var(list_value_CTR, ddof=1), np.var(list_value_KO, ddof=1)
                var = (((n1 - 1) * var_sample1) + ((n2 - 1) * var_sample2)) / (n1 + n2 - 2)
                std_error = np.sqrt(var * (1.0 / n1 + 1.0 / n2))

                if fold_change < 1:
                    fold_change = -1 / fold_change

                # calcula t statistics
                t = abs(mean_KO - mean_CTR) / std_error

                # p value para dos colas
                p_two = 2 * (1 - stats.t.cdf(x=t, df=8))

                # p value para una cola
                # p_one = 1 - stats.t.cdf(x=t, df=12)

                main_dict['Metabolite'].append(metabolite)
                main_dict['p-value'].append(p_two)
                main_dict['Fold Change'].append(fold_change)
                main_dict['Fenotipe 1'].append(fenotipe_ctr)
                main_dict['Fenotipe 2'].append(fenotipe_smp)
                list_value_CTR.clear()
                list_value_KO.clear()

            j = j + 1

            if j == (contador):
                y = y + 1
                j = y + 1
            if y == (contador - 1):
                proceso = False

        main_df1 = pd.DataFrame(main_dict)
        p_value_df = main_df1.loc[main_df1['p-value'] < alpha]
        print(main_df1)
        print(p_value_df)

    except Exception as e:
        print(e)

    return(main_df1)

# Llamada a la funcion data_integration


#data_integrada = data_integration(input_file, hmdbr_file, wikipathways, filter)
estats_1_all = control_all(estats_file, control, alpha)
#estats_all_all = all_all(estats_file, alpha)
