La función data_integration registra los datos de un archivo input (metabolomics.csv o proteomics.csv) y 
a partir de las entradas del archivo (metabolito/gen product-identificador) enlaza con 
los archivos hmdbr_lipidsmaps.rda y wikipathways.xlsx. 

El output genera los archivos links_file.json 

Las funciones controll_all y all_all comparan la expresion de las muestras por cada metabolito/gen product. Cada muestra
pertenece a una cepa, que se comparara con la cepa control, para obtener p-value y evaluar si la diferencia de expresion 
es significativa. 

control_all: Compara todas las muestras recogidas con una cepa control preseleccionada.
all_all: Compara todas las muestras recogidas entre ellas. 