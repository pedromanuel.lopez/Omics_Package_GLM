from setuptools import find_packages, setup
import pathlib


HERE = pathlib.Path(__file__).parent

VERSION = '0.0.1'
PACKAGE_NAME = 'OmicsData_Integration'
AUTHOR = 'Pedro Manuel Lopez Zarzuela'
AUTHOR_EMAIL = 'pedromanuel.lopez.zarzuela@gmail.com'
URL = 'https://github.com/caronteh'

LICENSE = 'BDS' #Pongo las licencias de las librerias usadas por ahora
DESCRIPTION = 'Libreria de integracion de datos multiomicos. El input recibido son archivos de tipo csv (proteomica o metabolomica), junto a BD como hmdbr_lipidsmap.rdad y wikipathways.xlsx'
LONG_DESCRIPTION = (HERE / "README.md").read_text(encoding='utf-8')
LONG_DESC_TYPE = "text/markdown"


#Paquetes necesarios para que funcione la libreria. Se instalaran a la vez si no lo tuvieras ya instalado
INSTALL_REQUIRES = ['pyreadr', 'pandas','openpyxl', 'numpy', 'scipy']

setup(
    name=PACKAGE_NAME,
    version=VERSION,
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    long_description_content_type=LONG_DESC_TYPE,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    url=URL,
    install_requires=INSTALL_REQUIRES,
    license=LICENSE,
    packages=find_packages(),
    include_package_data=True
)